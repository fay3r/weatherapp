package com.pl.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.pl.weatherapp.classes.MainWeatherClass;
import java.io.UnsupportedEncodingException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private EditText cityEdit;
    private MainWeatherClass weather;
    private String cityName;
    private static final String base = "http://api.openweathermap.org/";
    private static final String apiid = "749561a315b14523a8f5f1ef95e45864";
    private static final String units= "metric";
    private Intent intent;
    private Toast error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cityEdit = findViewById(R.id.cityName);
        loadData();
    }

    public void sendText(View view) throws UnsupportedEncodingException {
        if(haveNetwork()) {
            cityEdit = findViewById(R.id.cityName);
            cityName = cityEdit.getText().toString();
            intent = new Intent(this, WeatherDetails.class);
            intent.putExtra("CITY_NAME", cityName);

            Call<MainWeatherClass> call = findData(changer(cityName));
            call.enqueue(new Callback<MainWeatherClass>() {
                @Override
                public void onResponse(Call<MainWeatherClass> call, Response<MainWeatherClass> response) {
                    if (!response.isSuccessful()) {
                        System.out.println("Code: " + response.code());

                        error = Toast.makeText(getApplicationContext(), "Zła nazwa miasta", Toast.LENGTH_SHORT);
                        error.setGravity(Gravity.TOP, 0, 100);
                        error.show();

                        return;
                    }
                    weather = response.body();
                    intent.putExtra("WEATHER_CLASS", weather);
                    startActivity(intent);
                    saveData(cityName);
                }

                @Override
                public void onFailure(Call<MainWeatherClass> call, Throwable t) {
                }
            });
        }
        else{
            error = Toast.makeText(getApplicationContext(), getString(R.string.internet), Toast.LENGTH_SHORT);
            error.setGravity(Gravity.TOP, 0, 100);
            error.show();
        }

    }

    private boolean haveNetwork(){
        boolean have_WIFI= false;
        boolean have_MobileData = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for(NetworkInfo info:networkInfos){
            if (info.getTypeName().equalsIgnoreCase("WIFI"))if (info.isConnected())have_WIFI=true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE DATA"))if (info.isConnected())have_MobileData=true;
        }
        return have_WIFI||have_MobileData;
    }

    public static Call<MainWeatherClass> findData(String city){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(base).
                addConverterFactory(GsonConverterFactory.create()).build();

        JsonPlaceholderAPI jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);

        Call<MainWeatherClass> call = jsonPlaceholderAPI.getMainWeatherClassCall(city+",pl", apiid, units);

        return call;

    }

    private void saveData(String name){
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CITY_KEY",name);
        editor.apply();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences",MODE_PRIVATE);
        cityName = sharedPreferences.getString("CITY_KEY","x");
        cityEdit.setText(cityName);

    }

    private String changer(String x) {
        StringBuilder sb = new StringBuilder();
        int L = x.length();
        for (int i = 0; i < L; i++)
        {
            switch (x.charAt(i))
            {
                case 'ą': sb.append('a'); break;
                case 'ć': sb.append('c'); break;
                case 'ę': sb.append('e'); break;
                case 'ł': sb.append('l'); break;
                case 'ń': sb.append('n'); break;
                case 'ó': sb.append('o'); break;
                case 'ś': sb.append('s'); break;
                case 'ź': sb.append('z'); break;
                case 'ż': sb.append('z'); break;
                case 'Ą': sb.append('A'); break;
                case 'Ć': sb.append('C'); break;
                case 'Ę': sb.append('E'); break;
                case 'Ł': sb.append('L'); break;
                case 'Ń': sb.append('N'); break;
                case 'Ó': sb.append('O'); break;
                case 'Ś': sb.append('S'); break;
                case 'Ź': sb.append('Z'); break;
                case 'Ż': sb.append('Z'); break;
                default: sb.append(x.charAt(i)); break;
            }
        }
        return sb.toString();
    }
}