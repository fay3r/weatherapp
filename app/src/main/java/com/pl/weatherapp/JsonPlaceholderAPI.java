package com.pl.weatherapp;

import com.pl.weatherapp.classes.MainWeatherClass;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceholderAPI {
    @GET("data/2.5/weather")
    Call<MainWeatherClass> getMainWeatherClassCall(@Query("q") String q,
                                                   @Query("APPID") String APPID,
                                                   @Query("units") String units);
}
