package com.pl.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pl.weatherapp.classes.MainWeatherClass;

import com.squareup.picasso.Picasso;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WeatherDetails extends AppCompatActivity {

    private TextView weatherCity;
    private String iconUrl = "https://openweathermap.org/img/wn/";
    private String location;
    private TextView clock,temp,tempMax,tempMin,pressure,humidity;
    private MainWeatherClass weatherFromgson;
    private ImageView icon;
    private SwipeRefreshLayout swipeRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        Intent intent = getIntent();
        location = intent.getStringExtra("CITY_NAME");
        weatherFromgson =(MainWeatherClass) intent.getSerializableExtra("WEATHER_CLASS");
        iconUrl=iconUrl+weatherFromgson.getWeather()[0].getIcon()+"@2x.png";

        update(weatherFromgson);

        ScheduledExecutorService e= Executors.newSingleThreadScheduledExecutor();
        e.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                refreshData();
            }
        }, 0, 300, TimeUnit.SECONDS);

        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void refreshData() {
        Call<MainWeatherClass> call = MainActivity.findData(location);
        call.enqueue(new Callback<MainWeatherClass>() {
            @Override
            public void onResponse(Call<MainWeatherClass> call, Response<MainWeatherClass> response) {
                weatherFromgson=response.body();
                update(weatherFromgson);
            }

            @Override
            public void onFailure(Call<MainWeatherClass> call, Throwable t) {
                Toast error = Toast.makeText(getApplicationContext(), getString(R.string.internet), Toast.LENGTH_SHORT);
                error.setGravity(Gravity.TOP, 0, 100);
                error.show();
            }
        });
    }

    public void update(MainWeatherClass weather){
        weatherCity = findViewById(R.id.name);
        clock = findViewById(R.id.clock);
        temp =findViewById(R.id.tempInfo);
        tempMax = findViewById(R.id.maxInfo);
        tempMin = findViewById(R.id.minInfo);
        pressure = findViewById(R.id.pressureInfo);
        humidity = findViewById(R.id.humidityInfo);
        icon = findViewById(R.id.weatherIcon);
        clock.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

        Picasso.with(this).load(iconUrl).error(R.drawable.error).into(icon);
        weatherCity.setText(location);
        temp.setText(Double.toString(weather.getMain().getTemp())+" °C");
        tempMax.setText(Double.toString(weather.getMain().getTemp_max())+" °C");
        tempMin.setText(Double.toString(weather.getMain().getTemp_min())+" °C");
        pressure.setText(Integer.toString(weather.getMain().getPressure())+" hPa");
        humidity.setText(Integer.toString(weather.getMain().getHumidity())+" %");

    }
}

